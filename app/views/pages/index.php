<?php include APPROOT . '/views/inc/header.php'; ?>

<h1><?php echo $data['title']; ?></h1>

<p>This is a PHP Framework written from scratch, in a MVC style. It's main feature is the usage of PDO. <br>Use it to create web projects such as blogs, portals, forums, etc.<br> Check the docs for more info.</p>

<?php include APPROOT . '/views/inc/footer.php'; ?>