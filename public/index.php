<?php

// Show PHP Errors, 1=on 0=off
error_reporting(E_ALL);
ini_set('display_errors', 1);

include '../app/bootstrap.php';

// Init Core Library
$init = new Core;